resource "google_compute_network" "invia-network" {
  name                    = "invia"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "invia-subnetwork-subnet1" {
  name                     = "invia-subnet1"
  ip_cidr_range            = "10.0.0.0/24"
  network                  = "${google_compute_network.invia-network.self_link}"
  region                   = "us-central1"
  private_ip_google_access = true
}

resource "google_compute_subnetwork" "invia-subnetwork-subnet2" {
  name                     = "invia-subnet2"
  ip_cidr_range            = "10.1.0.0/24"
  network                  = "${google_compute_network.invia-network.self_link}"
  region                   = "us-central1"
  private_ip_google_access = true
}