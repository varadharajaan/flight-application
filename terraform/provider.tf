provider "google" {
  credentials = "${file("../credentials/account.json")}"
  project     = "invia-flightbooking"
  region      = "us-central1"
}